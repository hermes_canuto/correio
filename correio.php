<?php
class Correio
{
    protected $resultado = array ();
    
    function __construct()
    {
        $count = func_num_args();
        if ($count == 0) {
            return true;
        } else if ($count == 8) {
            $param = func_get_args();
            return $this->calcula($param[0], $param[1], $param[2], $param[3], $param[4], $param[5], $param[6], $param[7]);
        } else {
            return "Erro:";
        }
    }
    
    function calcula()
    {
        $param           = func_get_args();
        
        $cod_servico     = $param[0];
        $cep_origem      = $param[1];
        $cep_destino     = $param[2];
        $peso            = $param[3];
        $altura          = $param[4];
        $largura         = $param[5];
        $comprimento     = $param[6];
        $valor_declarado = $param[7];
        
        $cod_servico     = strtoupper($cod_servico);    
  
        if ( $cod_servico == 'SEDEX10')      { $cod_servico = 40215; }
        if ( $cod_servico == 'SEDEXACOBRAR') { $cod_servico = 40045; }
        if ( $cod_servico == 'SEDEX')        { $cod_servico = 40010; }
        if ( $cod_servico == 'PAC')          { $cod_servico = 41106; }
        
        $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx?nCdEmpresa=&sDsSenha=&sCepOrigem=" . $cep_origem . "&sCepDestino=" . $cep_destino . "&nVlPeso=" . $peso . "&nCdFormato=1&nVlComprimento=" . $comprimento . "&nVlAltura=" . $altura . "&nVlLargura=" . $largura . "&sCdMaoPropria=n&nVlValorDeclarado=" . $valor_declarado . "&sCdAvisoRecebimento=n&nCdServico=" . $cod_servico . "&nVlDiametro=0&StrRetorno=xml";
       
        $xml      = simplexml_load_file($correios);
        $_arr_    = array();
        
        $this->resultado = $_arr_;
        
        if ($xml->cServico->Erro == '0'):
        
            $_arr_['codigo']    = (string) $xml->cServico->Codigo;
            $_arr_['valor']     = (string) $xml->cServico->Valor;
            $_arr_['prazo']     = (string) $xml->cServico->PrazoEntrega;            
            $_arr_['erro']      = (string) $xml->cServico->MsgErro;
            
            $this->resultado = $_arr_;    
         
            return $_arr_;
        else:
            return false;
        endif;
    }
    
    public function getFrete()
    {
        return $this->resultado;
    }
    
    public function getValor()
    {
        return $this->resultado['valor'];
    }
    
    public function getCodigo()
    {
        return $this->resultado['codigo'];
    }
    
    public function getPrazo()
    {
        return $this->resultado['prazo'];
    }
    
    public function getErro(){
        return $this->resultado['erro'];
    }
    
    public static function tracking($codigo){
        
        $post = ['objetos' => $codigo , 'btnPesq' => 'Buscar'];
        
        $ch = curl_init('http://www2.correios.com.br/sistemas/rastreamento/resultado.cfm?');
        
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_REFERER, 'http://www2.correios.com.br/sistemas/rastreamento/');
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
        
        return $output = curl_exec($ch);
    }
    
}
?>