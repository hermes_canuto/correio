<?php 

include "correio.php";

/* 
 * 
cod_servico : ('SEDEX' ou 'PAC')
cep_origem: string sem "-"
cep_destino: string sem "-"
peso kg string exemplo um kilo = 1 , meio kilo 0.5
altura cm string
largura cm string
comprimento cm string
valor_declarado geralmente 0`

Exemplo de uso

Inicializa o objeto:
$frete = new Correio('pac', '11680000','82220000', '1','15', '22', '32', 0);

Retornos:

Retorna o codigo do serviço:
echo $frete->getCodigo()."<br>";

Retorna o valor:
echo $frete->getValor() ."<br>";

Retorna o prazo:
echo $frete->getPrazo() ."<br>";

*/

$frete = new Correio('SEDEX','11680000','82220000', '1', '15', '22', '32', '0');
echo  $frete->getCodigo()."<br>";
echo  $frete->getValor() ."<br>";
echo  $frete->getPrazo() ."<br>";
echo "------------------------------------------------------------------------<br>";
$frete2 = new Correio();
$frete2->calcula('PAC','09851130','82220000', '1', '15', '22', '32', '0'); 
echo  $frete2->getCodigo()."<br>";
echo  $frete2->getValor() ."<br>";
echo  $frete2->getPrazo() ."<br>";
echo  $frete2->getErro()  ."<br>";

echo "------------------------------------------------------------------------<br>";




?>