<?php

require_once ("iugu-php-master/lib/Iugu.php");

// troca a toke por desenvolvimento
Iugu::setApiKey("7202911c4534ef3be88c2a5ed77a8bac");

/*
 * Cria um cliente
 * nome, emial,cpf,nota string
 *
 * retorna o customer_id , identificador na plataforma
 */
function criaCliente($nome, $email, $cpf, $notas, $endereco, $numero, $complemento, $cidade, $estado, $pais, $cep)
{
    $return = Iugu_Customer::create(Array(
        "email" => $email,
        "name" => $nome,
        "cpf_cnpj" => $cpf,
        "notes" => $notas,
        "street" => $endereco,
        "number" => $numero,
        "complement" => $complemento,
        "city" => $cidade,
        "state" => $estado,
        "country" => $pais,
        "zip_code" => $cep
    ));
    
    return $return['id'];
}
/*
 * Altera um cliente
 * $customer_id,$nome, $email, $cpf, $notas, $endereco, $numero, $complemento, $cidade, $estado, $pais, $cep string
 *
 * retorna uma array
 */
function alteraCliente($customer_id,$nome, $email, $cpf, $notas, $endereco, $numero, $complemento, $cidade, $estado, $pais, $cep)
{

    $customer = Iugu_Customer::fetch($customer_id);
    $customer->name     = $nome;
    $customer->email    = $email;
    $customer->cpf_cnpj = $cpf;
    $customer->notes    = $notas;
    $customer->street   = $endereco;
    $customer->number   = $numero;
    $customer->complement = $complemento;
    $customer->city     = $cidade;
    $customer->state    = $estado;
    $customer->country  = $pais;
    $customer->zip_code = $cep;
    
    $customer->save()
    
    return $customer;
}

/*
 * Retorna os dados do cliente em formato array
 * customer_id string
 */
function obtemCliente($customer_id)
{
    $retorno = Iugu_Customer::fetch($customer_id);
    return $retorno;
}

/*
 * Deleta o cliente na plataforma, desde que n�o tenha historico de pagamento
 * customer_id string
 */
function deleteCliente($customer_id)
{
    $customer = Iugu_Customer::fetch($customer_id);
    $customer->delete();
}

/*
 * Gera um boleto
 * $customer_id, $ddd, $telefone string
 * $itens array no seguinte formato exemplo
 * 
 * array(
        Array("description" => "Caderno","quantity" => "1", "price_cents" => "10000"),
        Array("description" => "bolsa","quantity" => "2", "price_cents" => "40000"),
        Array("description" => "queijo","quantity" => "1", "price_cents" => "50000"),    
    );
 * 
 * 
 * 
 */
    function geraBoleto($customer_id, $ddd, $telefone, $itens)
    {
        $cliente = Iugu_Customer::fetch($customer_id);

        $dados_cliente = Array(

            "name" => $cliente['name'],
            "phone_prefix" => "11",
            "phone" => "992627907",
            "email" => $cliente['email'],

            "address" => Array(
                "street" => $cliente['street'],
                "number" => $cliente['numbe'],
                "city" => $cliente['city'],
                "state" => $cliente['state'],
                "country" => $cliente['country'],
                "zip_code" => $cliente['zip_code']
            )
        );

        $boleto = Iugu_Charge::create(array(

            "customer_id" => $customer_id,
            "method" => "bank_slip",
            "payer" => $dados_cliente,
            "items" => $itens
        ));

        return $boleto;
    }

/*
 * Gera um pagamento com cart�o de credito
 * $customer_id, $ddd, $telefone , $token string
 * $token recebido pela plataforma, s� pode ser usado uma vez
 * $itens array no seguinte formato exemplo
 *
 * array(
     Array("description" => "Caderno","quantity" => "1", "price_cents" => "10000"),
     Array("description" => "bolsa","quantity" => "2", "price_cents" => "40000"),
     Array("description" => "queijo","quantity" => "1", "price_cents" => "50000"),
   );
 *
 *
 *
 */
   function geraCC($customer_id, $ddd, $telefone, $itens,$token)
   {
    $cliente = Iugu_Customer::fetch($customer_id);
    
    $dados_cliente = Array(

        "name" => $cliente['name'],
        "phone_prefix" => "11",
        "phone" => "992627907",
        "email" => $cliente['email'],
        
        "address" => Array(
            "street" => $cliente['street'],
            "number" => $cliente['numbe'],
            "city" => $cliente['city'],
            "state" => $cliente['state'],
            "country" => $cliente['country'],
            "zip_code" => $cliente['zip_code']
        )
    );
    
    $cc = Iugu_Charge::create(array(        
        "customer_id" => $customer_id,
        "token" => $token,
        "payer" => $dados_cliente,
        "items" => $itens
    ));
    
    return $cc;
}

/*
 * Gera um pagamento com cart�o de credito
 * $customer_id, $ddd, $telefone , $token string
 * $token recebido pela plataforma, s� pode ser usado uma vez
 * $itens array no seguinte formato exemplo 
 *
 * array(
     Array("description" => "Caderno","quantity" => "1", "price_cents" => "10000"),
     Array("description" => "bolsa","quantity" => "2", "price_cents" => "40000"),
     Array("description" => "queijo","quantity" => "1", "price_cents" => "50000"),
   );
 *
 * $meio_de_pagamento_id string
 *
 */
   function geraCCM($customer_id, $ddd, $telefone, $itens, $meio_de_pagamento_id  ){

    cliente = Iugu_Customer::fetch($customer_id);
    
    $dados_cliente = Array(

        "name" => $cliente['name'],
        "phone_prefix" => "11",
        "phone" => "992627907",
        "email" => $cliente['email'],
        
        "address" => Array(
            "street" => $cliente['street'],
            "number" => $cliente['numbe'],
            "city" => $cliente['city'],
            "state" => $cliente['state'],
            "country" => $cliente['country'],
            "zip_code" => $cliente['zip_code']
        )
    );

    $ccm = Iugu_Charge::create(array(        
        "customer_id" => $customer_id,
        "customer_payment_method_id" => $meio_de_pagamento_id,
        "payer" => $dados_cliente,
        "items" => $itens
    ));
    
    return $ccm;

}

/*
 * retorna dados de uma fatura 
 * $invoice_id string
 * return um array 
 */
function retornaFatura($invoice_id){

    $invoice = Iugu_Invoice::fetch($invoice_id);
    return $invoice;
}
/*
 * apaga uma fatura 
 * $invoice_id string 
 */
function apagaFatura($invoice_id){

 $invoice = Iugu_Invoice::fetch($invoice_id);
 $invoice->delete();

}
/*
 * Cancela um fatura
 * $invoice_id string 
 */
function cancelaFatura($invoice_id){

    $invoice = Iugu_Invoice::fetch($invoice_id);
    $invoice->cancel();
    
}

/*
 * cria meio de pagamento / salva o cart�o no gateway
 * $customer_id,$token,$descricaod string 
 * retorna uma array
 */

function crieMeiodepagamento($customer_id,$token,$descricao){

    $customer = Iugu_Customer::fetch( $customer_id )

    $payment_method = $customer->payment_methods()->create(Array(
        "description" => $descricao,
        "token" => $token
    ));
    return $payment_method;

}
?>